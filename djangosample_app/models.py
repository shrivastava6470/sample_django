from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Client_data(models.Model):
    Client_name=models.CharField(max_length=25)
    Client_email=models.EmailField()
    Client_Image=models.ImageField(upload_to='media/Client_Image/', blank=True,null=True)
    Client_points=models.IntegerField()

    Contact_person_name=models.CharField(max_length=25,blank=True,null=True)

    Client_mobile_number=models.CharField(max_length=25,)
    Client_Uniqe_id=models.CharField(max_length=25,blank=True,null=True)
    Client_address=models.CharField(max_length=150,)
    #Organisationsp_no=models.CharField(max_length=25)
    which_user=models.OneToOneField(User,on_delete=models.CASCADE,related_name='Client_user',)
    #Organisations_Created_at=models.DateTimeField(auto_now_add=True,blank=True,null=True)
    is_active = models.BooleanField(default=False,blank=True,null=True)
    is_email_verify=models.BooleanField(default=False,blank=True,null=True)

    is_subscribe=models.BooleanField(default=False,blank=True,null=True)

    createdAt = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    createdBy = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True,related_name='created_By')
    updatedAt = models.DateTimeField(auto_now=True, blank=True, null=True)
    otp_forgot_password=models.CharField(max_length=25,blank=True,null=True)


class Applications(models.Model):

    Application_name=models.CharField(max_length=30,null=True,blank=True)
    Application_Catgory=models.CharField(max_length=30,null=True,blank=True)
    Application_short_description=models.CharField(max_length=30,null=True,blank=True)
    point=models.IntegerField()

    Application_img=models.ImageField(upload_to='media/Application_img/', blank=True,null=True)
    #Group_Id=models.ForeignKey(Application_Group,on_delete=models.CASCADE)
    Text=models.TextField()


