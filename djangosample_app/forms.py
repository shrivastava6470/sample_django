from django import forms
from .models import Client_data

#Form ->
#1. Form Validation (liek mobile number is valid or not etc.)
#2. and other validations as well.

class SignUpForm(forms.ModelForm):
    #password=forms.CharField()
    #confrom_password=forms.CharField()
    password = forms.CharField(label='Password',widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confrom Password',widget=forms.PasswordInput)

    class Meta:
        model = Client_data
        fields = ("Client_name",'Client_email','Client_Image','Contact_person_name','Client_mobile_number','Client_address')
    def clean_password2(self):
            cd = self.cleaned_data
            if cd['password'] != cd['password2']:
                raise forms.ValidationError("Passwords don't match.")
            return cd['password2']




#Main URL -> App URL -> View _> FOrm -> template -> Form -> View -> Form Save -> Main URL