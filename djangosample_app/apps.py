from django.apps import AppConfig


class DjangosampleAppConfig(AppConfig):
    name = 'djangosample_app'
