# Generated by Django 2.2.5 on 2020-05-22 13:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Applications',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Application_name', models.CharField(blank=True, max_length=30, null=True)),
                ('Application_Catgory', models.CharField(blank=True, max_length=30, null=True)),
                ('Application_short_description', models.CharField(blank=True, max_length=30, null=True)),
                ('Application_img', models.ImageField(blank=True, null=True, upload_to='media/Application_img/')),
                ('Text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Client_data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Client_name', models.CharField(max_length=25)),
                ('Client_email', models.EmailField(max_length=254)),
                ('Client_Image', models.ImageField(blank=True, null=True, upload_to='media/Client_Image/')),
                ('Client_points', models.IntegerField()),
                ('Contact_person_name', models.CharField(blank=True, max_length=25, null=True)),
                ('Client_mobile_number', models.CharField(max_length=25)),
                ('Client_Uniqe_id', models.CharField(blank=True, max_length=25, null=True)),
                ('Client_address', models.CharField(max_length=150)),
                ('is_active', models.BooleanField(blank=True, default=False, null=True)),
                ('is_email_verify', models.BooleanField(blank=True, default=False, null=True)),
                ('is_subscribe', models.BooleanField(blank=True, default=False, null=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('updatedAt', models.DateTimeField(auto_now=True, null=True)),
                ('otp_forgot_password', models.CharField(blank=True, max_length=25, null=True)),
                ('createdBy', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_By', to=settings.AUTH_USER_MODEL)),
                ('which_user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='Client_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
